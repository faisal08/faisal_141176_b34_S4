<?php
$str="is your name o'reilly";
echo addslashes($str);//RETURN STRING WITH SLASHED
//OUTPUT:is your name o\'reilly
echo "<br>";
$x="piece1 piece2 piece3 piece4 ";
$y=explode(" ",$x);//RETURN AN ARRAY AND SPLIT THE STRING
echo $y[2];

//OUTPUT:PIECE3
echo "<br>";
$str = "A 'quote' is <b>bold</b>";
echo htmlentities($str);
echo  "<br>";
$var2=array('this' ,'is' , 'a', 'string');
echo implode("+",$var2);//JOIN ARRAY ELEMENT WITH STRING AND RETURN TYPE ALSO STRING
//OUTPUT:THIS+IS+A+STRING
echo "<br>";
$var3="    hello world";
$var4=ltrim($var3);
var_dump($var4);//STRIP WHITESPACES ...NOT CALCULATE WHITESPACE
echo "<br>";
$var5= "hello world         ";
$var6= rtrim($var5);//STRIP WHITESPACES FROM THE RIGHT END
var_dump($var6);
echo "<br>";
$var7="wellcome\n this is my html document";//INSERT HTML LINE BREAK BEFORE ALL NEWLINE
echo nl2br($var7);
/*OUTPUT:WELLCOME
THIS IS MY HTML DOCUMENT*/
echo "<br>";
$input="alien";
echo str_pad($input,10 ,".");
//OUTPUT:ALIEN........          ALLOCATE OR PTINT 10 CHARACTER
echo "<br>";
echo str_pad($input,20,".",str_pad_left);//pad a string of certain length with another string
//OUTPUT:............ALIEN
echo "<br>";
$repeat="repeat me";
echo str_repeat($repeat,10);//repeat a string as the number specified
//OUTPUT:REPEAT ME REPEAT ME ....10TIMES
echo "<br>";
$str2="Hello world";
$str3= str_split($str2);//RETURN STRING INTO ARRAY OR CONVERT STRING TO ARRAY
print_r($str3);
/*ARRAY(
 'H'
 'E'
 'L'
 ........)UNTILL THE STRING COMPLETED
*/
echo "<br>";
$str4=str_split($str2,3);
print_r($str4);
/*OUTPUT:
'HEL'
'LOW'
....CONTINUE USING 3 CHARACTER UNTILL STRING IS COMPLETED
*/
echo "<br>";
$str6="<p>thlis is for paragraphp</p> <b>bold line</b>";//STRIP HTML AND PHP TAG
echo strip_tags($str6);
//OUTPUT:THIS IS FOR PARAGRAPH BOLD LINE
echo "<br>";
echo strip_tags($str6,'<p>');
echo "<br>";
$str7="evaluate the length of the string";
echo strlen($str7);
// OUTPUT:33
echo "<br>";
$str8="lower to upper";
echo strtoupper($str8);
//OUTPUT:LOWER TO UPPER
echo "<br>";
$str9="UPPER TO LOWER";
echo  strtolower($str9);
//OUTPUT:upper to lower
echo "<br>";
$str10="this is a test";
echo substr_count($str10,'is');
echo "<br>";
$str11="use the ucfirstFunction";
echo ucfirst($str11);//Make the first string uppercase
//OUTPUT:Use The UcfirstFunction
echo"<br>";
$str12="strng replace";
echo substr_replace($str12,'i',4);//REPLACE A STRING WITH THE EXISTING STRING INTO THE SPECIFIED LOCATION
//OUTPUT:STRI  AT POSITION 4 I IS ADDED
echo "<br>";
$str13="hello";
echo strrev($str13);
echo "<br>";
$str15="\t\nstrip space";
echo $str15;
echo "<br>";
echo trim($str15);
echo "<br>";
echo substr_compare("abcdef",'bc',2,3);
?>
